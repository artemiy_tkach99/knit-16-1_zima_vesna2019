import java.util.ArrayList;

class ArrayNumbers{
    private ArrayList<Integer> list;

    ArrayNumbers()
    {
        list = new ArrayList<Integer>();
        for(int i = 0; i <= 20; i++)
            list.add(i);
    }

    ArrayList<Integer> getRange(int min, int max){
        var newList = new ArrayList<Integer>();
        for (int num : list) {
            if (num >= min && num <= max){
                newList.add(num);
            }
        }

        return newList;
    }
}

public class Main {

    public static void main(String[] args) {
        var nums = new ArrayNumbers();
        var list = nums.getRange(8, 16);
        for (var num : list) {
            System.out.println(num);
        }
    }

}
