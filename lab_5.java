class Street {

    private String name;

    public Street(String name){
        this.name = name;
    }

    public void display(){
        System.out.println("Name: " + name);
    }
}
class Avenue extends Street{

    private int numberOfIntersections;
    private int lastRenovation;

    public Avenue(String name, int numberOfIntersections, int lastRenovation) {

        super(name);
        this.numberOfIntersections = numberOfIntersections;
        this.lastRenovation = lastRenovation;
    }

    @Override
    public void display(){
        super.display();
        System.out.println("Intersections: " + numberOfIntersections);
        System.out.println("Last renovation: " + lastRenovation);
    }
}

class Alley extends Street{

    private int pollingStationNumber;

    public Alley(String name, int pollingStationNumber) {

        super(name);
        this.pollingStationNumber = pollingStationNumber;
    }

    @Override
    public void display(){
        super.display();
        System.out.println("Polling station number: " + pollingStationNumber);
    }

}

public class Main {

    public static void main(String[] args) {
        var test = new Avenue("Butyrina", 9, 1995);
        test.display();

        var test2 = new Alley("Mazepy", 7);
        test2.display();
    }

}
